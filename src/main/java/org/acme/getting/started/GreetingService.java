package org.acme.getting.started;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.ZonedDateTime;

@ApplicationScoped
public class GreetingService {

  @Inject
  @Channel("action-event")
  protected Emitter<Action> actionEmitter;

  public void sendAction() {
    Action ac = new Action("Action at " + ZonedDateTime.now());
    actionEmitter.send(ac);
  }


}
