package org.acme.getting.started;


import io.smallrye.reactive.messaging.annotations.Blocking;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class Observer {


  @Blocking
  @Incoming("action-event")
  public void doSomeAction(Action action){
    System.out.printf("Action fired [%s]\n",action.description);
  }

}
